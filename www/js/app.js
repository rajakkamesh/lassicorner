// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers'])

.run(function($ionicPlatform, $ionicPopup) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    /*if(window.Connection) {
                if(navigator.connection.type == Connection.NONE) {
                    $ionicPopup.confirm({
                        title: "Internet Disconnected",
                        content: "The internet is disconnected on your device."
                    })
                    .then(function(result) {
                        if(!result) {*/
                            /*ionic.Platform.exitApp();*/
             /*               navigator.app.exitApp();
                        }
                    });
                }
            }*/


  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.home', {
    url: '/home',
    views: {
      'menuContent': {
        templateUrl: 'templates/home.html'
      }
    }
  })

  .state('app.test', {
    url: '/test',
    views: {
      'menuContent': {
        templateUrl: 'templates/test.html',
            controller: 'TestCtrl'
      }
    }
  })

  .state('app.franchiseeLogin', {
    url: '/franchiseeLogin',
    views: {
      'menuContent': {
        templateUrl: 'templates/franchisee/login.html',
        controller: 'franLoginCtrl'
      }
    }
  })

   .state('app.franchisor', {
    url: '/franchisorHome',
    views: {
      'menuContent': {
        templateUrl: 'templates/franchisor/home.html',
        controller: 'franchisorHomeCtrl'
      }
    }
  })

   .state('app.processingOrder', {
    url: '/processingOrder',
    views: {
      'menuContent': {
        templateUrl: 'templates/franchisor/processingOrders.html',
        controller: 'processingOrderCtrl'
      }
    }
  })

  .state('app.facebookGoogleLogin', {
    url: '/facebookGoogleLogin',
    views: {
      'menuContent': {
        templateUrl: 'templates/orderFood/login.html',
        controller: 'FacebookGoogleCtrl'
      }
    }
  })


  .state('app.location', {
    url: '/location',
    views: {
      'menuContent': {
        templateUrl: 'templates/franchisee/location.html',
        controller: 'LocationCtrl'
      }
    }
  })

/*  .state('app.googleLogin', {
    url: '/googleLogin',
    views: {
      'menuContent': {
        templateUrl: 'templates/googleLogin.html',
        controller: 'GoogleCtrl'
      }
    }
        })*/

/*    .state('app.facebookLogin', {
    url: '/facebookLogin',
    views: {
      'menuContent': {
        templateUrl: 'templates/facebookLogin.html',
        controller: 'FacebookCtrl'
      }
    }
  })
*/

    .state('app.selectOutlet', {
    url: '/selectOutlet',
    views: {
      'menuContent': {
        templateUrl: 'templates/orderFood/selectOutlet.html',
        controller: 'LocationCtrl'
      }
    }
  })

    .state('app.showFranReceipt', {
    url: '/showFranReceipt',
    views: {
      'menuContent': {
        templateUrl: 'templates/franchisee/showFranReceipt.html',
        controller: 'ShowFranReceiptCtrl'
      }
    }
  })

  .state('app.placeOrder', {
    url: '/placeOrder',
    views: {
      'menuContent': {
        templateUrl: 'templates/orderFood/placeOrder.html',
        controller: 'OnlinePlaceOrderCtrl'
      }
    }
  })

  .state('app.paymentSelection', {
    url: '/paymentSelection',
    views: {
      'menuContent': {
        templateUrl: 'templates/franchisor/paymentSelection.html',
        controller: 'paymentSelectionCtrl'
      }
    }
  })


  .state('app.placeOrderFranchisee', {
    url: '/placeOrderFranchisee',
    views: {
      'menuContent': {
        templateUrl: 'templates/franchisee/placeOrderFranchisee.html',
        controller: 'OnlinePlaceOrderCtrlFranchisee'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/home');
});
