 angular.module('starter.controllers', ['ngCordova'])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {
  
})


.controller('TestCtrl', function($scope, $timeout, $http, $ionicLoading) {

		  /*$scope.hide = function(){
		        $ionicLoading.hide();
		  };

	  $scope.start = function(){

	  		$ionicLoading.show({
		      template: '<ion-spinner icon="spiral"></ion-spinner>'
		    });
	  		$http({
			    method:'GET',
			    url:'https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=40.6655101,-73.89188969999998&destinations=40.6905615%2C-73.9976592'
			}).then(function(response){
			    //success
			    //do something with the response
			    var res = JSON.stringify(response);
			    
			    $scope.distance = JSON.parse(res).data.rows[0].elements[0].distance.text;
			    console.log($scope.distance);

			}, function(response){
			    //error
				console.log('ERROR OCCURED IN CALCULATING DISTANCE');		    
			});

			   $ionicLoading.hide(); 
	  };*/

		
		$scope.openInAppBrowser = function()
		{
		 // Open in app browser
		 window.open('http://admin.lassicorner.com/ccavenue/index.htm','_blank'); 
		};	  
		
	  
  
})

.controller('paymentSelectionCtrl', function($scope, $timeout, $http, $ionicLoading) {

		  
		
		$scope.openInAppBrowser = function()
		{
		 console.log('cashOnDeliverySelected');
		 window.open('http://admin.lassicorner.com/index.htm','_blank'); 
		};	  

		$scope.cashOnDelivery = function()
		{
		 console.log('cashOnDeliverySelected');
		};	  
		
		
	  
  
})


/*.service('UserService', function() {
// For the purpose of this example I will store user data on ionic local storage but you should save it on a database
  var setUser = function(user_data) {
    window.localStorage.starter_google_user = JSON.stringify(user_data);
  };
  var getUser = function(){
    return JSON.parse(window.localStorage.starter_google_user || '{}');
  };
  return {
    getUser: getUser,
    setUser: setUser
  };
});*/



.controller('FacebookGoogleCtrl', function($scope, $state, $ionicLoading) {

	 // This method is executed when the user press the "Sign in with Google" button
  $scope.googleSignIn = function() {
    $ionicLoading.show({
      template: 'Logging in...'
    });
    window.plugins.googleplus.login(
      {},
      function (user_data) {
        // For the purpose of this example I will store user data on local storage

        console.log('USER_DATA: '+user_data);
        /*UserService.setUser({
          userID: user_data.userId,
          name: user_data.displayName,
          email: user_data.email,
          picture: user_data.imageUrl,
          accessToken: user_data.accessToken,
          idToken: user_data.idToken
        });*/
        
        $ionicLoading.hide();
        $state.go('app.selectOutlet');
      },
      function (msg) {
        $ionicLoading.hide();
      }
    );
  }
  /* google login*/

  /*faceboook*/
   $scope.show = function() {

			$ionicLoading.show({
			      template: '<p>Loading...</p><ion-spinner></ion-spinner>'

			    });
			setTimeout(function(){
			    console.log("THIS IS");			    
			    $scope.hide();
			    $state.go('app.selectOutlet');
			}, 2000);
			


			
		    
		  };

		  $scope.hide = function(){
		        $ionicLoading.hide();
		  };

		  $scope.login = function() {
		    // Start showing the progress
		    $scope.show($ionicLoading);
		    // Do the call to a service using $http or directly do the call here
		    Service.service($scope, $http).success(function(data) {
		      // Do something on success for example if you are doing a login
		        var alertPopup = $ionicPopup.alert({
		            title: 'Login successful!',
		        });
		    }).error(function(data) {
		      // Do something on error
		        var alertPopup = $ionicPopup.alert({
		            title: 'Login failed!',
		            template: 'Please check your credentials!'
		        });
		    }).finally(function($ionicLoading) { 
		      // On both cases hide the loading
		      $scope.hide($ionicLoading);  
		    });  
		  }


		  /*facebook*/


})




.controller('franLoginCtrl', function($state, $scope, $ionicModal, $timeout, $ionicPopup, $http, $ionicLoading) {


	 /*$scope.login = function(username,password,option){
		   $scope.username = username;
		   $scope.password = password;
		   $scope.option = option;

		   console.log($scope.username);
		   console.log($scope.password);
		   console.log($scope.option);

		}*/



	  $scope.showOutlet = function(outlet) {	  
	  	     
	  $scope.selectedOutlet = outlet; 
	  window.localStorage.setItem( "outlet",  $scope.selectedOutlet);
	  	 $scope.outletSelector = {
            'display': 'block'
          }
      };
      

      /*forgot password popup*/
         $scope.showForgotPopup = function() {
      $scope.data = {}
    
      // Custom popup
      var myPopup = $ionicPopup.show({
         template: '<input type = "text" ng-model = "data.username">',
         title: 'Enter username below to reset password',         
         scope: $scope,
			
         buttons: [
            { text: 'Cancel' }, {
               text: '<b>Submit</b>',
               type: 'button-assertive',
               onTap: function(e) {
						
                  if (!$scope.data.username) {
                     //don't allow the user to close unless he enters model...
                     e.preventDefault();
                     console.log($scope.data.username);
                  } else {
                     return $scope.data.username;
                  }
               }
            }
         ]
      });

      myPopup.then(function(res) {
         /*console.log(res);*/
         			$http({
				    method:'GET',
				    url:'http://admin.lassicorner.com/services/setPasswordResetStatus.php?username='+res+''
						}).then(function(response){
						   if (response.data == 'success') {
						   	$ionicLoading.show({ template: 'Password reset request submitted, Please contact administrator for more information', noBackdrop: true, duration: 3000 });
						   }
						   else{
						   	$ionicLoading.show({ template: 'No account associated with the username, please contact administrator', noBackdrop: true, duration: 3000 });
						   }
						}, function(response){						    	    
							console.log(response.data);
						});
         
      });    
   };
   /*forgot password popup*/






	 $scope.showAgreement = function(username,password,option) {
	           // When button is clicked, the popup will be shown...

	      $scope.data = {}

	      $scope.username = username;
		   $scope.password = password;
		   $scope.option = option;
		   


		   console.log("options value: " +$scope.username);
		   console.log("options value: " +$scope.password);
		   console.log("options value: " +$scope.option);
		   

		   


		   
	    	
	      // Custom popup
	      var myPopup = $ionicPopup.show({
	         template: '<i style="color:#990000;">By clicking Submit you agrees to the terms and conditions of LassiCorner.</i>',
	         title: 'Terms & Agreement',
	         /*subTitle: 'Subtitle',*/
	         scope: $scope,
				
	         buttons: [
	             {
	               text: '<i class="icon ion-checkmark-circled"></i><b>&nbsp;&nbsp;Submit</b>',
	               type: 'my-button',
	               onTap: function(e) {

	               		/*check pass*/
	               		$http({
				    method:'GET',
				    url:'http://admin.lassicorner.com/services/checkLogin.php?username='+$scope.username+'&password='+$scope.password+'&option='+$scope.option+''
						}).then(function(response){
						    //success						    
						  	
						  	
						    if (response.data.replace(/[\s]/g, '') == 'successFranchisee') {
						    	window.localStorage.setItem( "username",  $scope.username);
						    	$state.go('app.placeOrderFranchisee');

						    }
						    else if (response.data.replace(/[\s]/g, '') == 'successFranchisor') {
						    	$state.go('app.franchisor');
						    }
						    else{
						    	$scope.errorSelector = {
					            'display': 'block'
					          } 
						    }
						    
						  
						}, function(response){
						    //error
							console.log('ERROR OCCURED');		    
							console.log(response.data);
						});
	               		/*check pass*/

	               		/*$state.go('app.franchisor');*/

/*	                  		$state.go('app.placeOrderFranchisee');*/
	                                       
	               }
	            }
	         ]
	      });

	      myPopup.then(function(res) {
	         console.log('Tapped!');
	      });    
	   
	  };
	  
  
})


.controller('LocationCtrl', function($scope, $state, $ionicLoading, $cordovaGeolocation, $http) {


	    /*key used of jan jeevan sanstha project*/
  		/* AIzaSyDh8qtBfkcIAeJ-yC4aIl3ki_Adt7u57Cg */


  		
		  var options = {timeout: 10000, enableHighAccuracy: true};

		  		$scope.showOutlet = function(outlet) {	  
						  
						  $scope.selectedOutlet = outlet; 
						  window.localStorage.setItem("outletOrder",$scope.selectedOutlet);
						  console.log('SELECTED FROM DROPDOWN');
						  /*console.log(JSON.parse(res).data);*/
						  	 
					      };
		 
		  $cordovaGeolocation.getCurrentPosition(options).then(function(position){


		  	/*loading*/
		  		$scope.show = function() {

				$ionicLoading.show({
				      template: '<p>Your current location is far from outlet, please select your nearest outlet from dropdown...</p><ion-spinner></ion-spinner>'
				    });
			  };

			  $scope.hide = function(){
			        $ionicLoading.hide();
			  };

		  	/*loading*/
		 
		  	$scope.show();

			$scope.locationList = ['12.9592,77.6974','12.9260,77.6762','13.0184,77.6781','12.9719,77.5299','12.9795,77.7638','12.840711, 77.676369'];
			$scope.distanceMatrix = [];

			//console.log('https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins='+position.coords.latitude+','+position.coords.longitude+'&destinations='+$scope.locationList[0]+'');



		    /*to compare the distance*/
		    for (var i = $scope.locationList.length - 1; i >= 0; i--) {		    	
		    
		    	$http({
				    method:'GET',
				    url:'https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins='+position.coords.latitude+','+position.coords.longitude+'&destinations='+$scope.locationList[i]+''
				}).then(function(response){
				    //success
				    //do something with the response
				    var res = JSON.stringify(response);
				    
				    $scope.distance = JSON.parse(res).data.rows[0].elements[0].distance.text.slice(0, -3).replace(',','');
				    //console.log($scope.distance);
				    if ($scope.distance < 2) {
				    	console.log('IN_RANGE')
				    	/*console.log(JSON.parse(res).data.rows[0].elements[0].distance.text.slice(0, -3));*/
				    	$scope.showOutlet = function(outlet) {	  
						  
						  $scope.selectedOutlet = outlet; 
						  window.localStorage.setItem("outletOrder",$scope.selectedOutlet);
						  console.log('IN RANGE');
						  /*console.log(JSON.parse(res).data);*/
						  	 
					      };
				    }



				    
				    /*else{
				    	$scope.distanceMatrix.push(parseInt($scope.distance));
				    	window.localStorage.setItem("outletOrder",$scope.selectedOutlet);
				    	console.log('NOT IN RANGE');
				    }*/




				}, function(response){
				    //error
					console.log('ERROR OCCURED IN CALCULATING DISTANCE');		    
				});




			}


			setTimeout(function(){
				    		    
				    $scope.hide();				    
				}, 2000);


			console.log($scope.distanceMatrix);

			$scope.min = 0;
			for (var i = $scope.distanceMatrix.length - 1; i >= 0; i--) {

				console.log($scope.distanceMatrix[i]);
				console.log($scope.distanceMatrix[i-1]);


				if ($scope.distanceMatrix[i] < $scope.distanceMatrix[i-1] ) {
					$scope.min = $scope.distanceMatrix[i];
					console.log('$scope.distanceMatrix[i]');
				}				
				else{
					$scope.min =
					 $scope.distanceMatrix[i-1];
					 console.log('$scope.distanceMatrix[i-1]');
				}
			}
			
		    /*to compare the distance*/



		    /*var mapOptions = {
		      center: latLng,
		      zoom: 15,
		      mapTypeId: google.maps.MapTypeId.ROADMAP
		    };*/
		 
		 /*   $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);*/

		      //Wait until the map is loaded
			/*google.maps.event.addListenerOnce($scope.map, 'idle', function(){
			 
			  var marker = new google.maps.Marker({
			      map: $scope.map,
			      animation: google.maps.Animation.DROP,
			      position: latLng
			  });     
			 
			  var infoWindow = new google.maps.InfoWindow({
			      content: "You are here"
			  });
			 
			  google.maps.event.addListener(marker, 'click', function () {
			      infoWindow.open($scope.map, marker);
			  });
			 
			});*/
		 
		  }, function(error){
		    console.log("Could not get location");
		  });



		


})

/* SelectOutletCtrl */

/*.controller('SelectOutletCtrl', function($scope, $timeout) {

	console.log('SelectOutletCtrl');

  
})*/


.controller('franchisorHomeCtrl', function($scope, $timeout) {
	console.log('franchisorHomeCtrl');

  
})

.controller('processingOrderCtrl', function($scope, $timeout, $ionicPopup, $http, $state) {
	console.log('processingOrderCtrl');


				$scope.fetchOrders = function(date, amount) {
					$scope.date = date;
					$scope.amount = amount;


					var date = new Date($scope.date),
			        mnth = ("0" + (date.getMonth()+1)).slice(-2),
			        day  = ("0" + date.getDate()).slice(-2);

				    
				    $scope.items = [];
					$http({
				    method:'GET',				    
				    url:'http://admin.lassicorner.com/services/getOrdersByDateAmount.php?date='+[ day, mnth, date.getFullYear() ].join("-")+'&amount='+$scope.amount+''
						}).then(function(response){
						    //success
							if (response.data.length == 0) {
								var alertPopup = $ionicPopup.alert({
						         title: 'Orders',
						         template: 'No Orders Found!!!'
						      });	
								
							}
							else{
								console.log(response.data);
								//$scope.items = [];
								$scope.items = response.data;
								console.log($scope.items.length);
							}

						}, function(response){
						    //error						
							var alertPopup = $ionicPopup.alert({
						         title: 'Order',
						         template: 'Error Occured'
						      });	    
							
						});


				  };

				
				$http({
			    method:'GET',				    
			    url:'http://admin.lassicorner.com/services/getProcessingOrders.php'
					}).then(function(response){
					    //success
						if (response.data.length == 0) {
							var alertPopup = $ionicPopup.alert({
					         title: 'Orders',
					         template: 'No Processing Orders'
					      });	
							
						}
						else{
							console.log(response.data);
							$scope.items = response.data;
						}

					}, function(response){
					    //error						
						var alertPopup = $ionicPopup.alert({
					         title: 'Order',
					         template: 'Error Occured'
					      });	    
						
					});

  
})


.controller('ShowReceiptCtrl', function($scope, $timeout) {

	console.log('ShowReceipt');

  
})


.controller('ShowFranReceiptCtrl', function($scope, $timeout, $http, $ionicPopup, $state) {
	
			console.log('ShowFranReceiptCtrl');

			/*console.log(window.localStorage.getItem('orders'));
			console.log(window.localStorage.getItem('outlet'));*/

			$scope.items = [];
			$scope.price = [];
			$scope.total = 0.0;


			$scope.outlet = window.localStorage.getItem('outlet');
			$scope.orderDate = new Date();

			/*console.log((window.localStorage.getItem('orders').split(',')[0]).split(':')[1]);*/

			for (var i = (window.localStorage.getItem('orders').split(',')).length - 1; i >= 0; i--) {
				/*console.log((window.localStorage.getItem('orders').split(',')[0]).split(':')[1]);				*/
				$scope.items.push((window.localStorage.getItem('orders').split(',')[i]).split(':')[0]);
				$scope.price.push((window.localStorage.getItem('orders').split(',')[i]).split(':')[1]);

				$scope.total = $scope.total + parseFloat((window.localStorage.getItem('orders').split(',')[i]).split(':')[1]);
				$scope.total = Math.round($scope.total*100)/100;
				

			}

			console.log($scope.items);
			console.log($scope.price);
			console.log($scope.total);

			$scope.confirm = function() {	  	        
		        		        
		        $http({
			    method:'GET',				    
			    url:'http://admin.lassicorner.com/services/confirmOrder.php?id='+window.localStorage.getItem('id')+''
					}).then(function(response){
					    //success
						console.log('OrderConfirmed ');
						var alertPopup = $ionicPopup.alert({
					         title: 'Order',
					         template: 'Order Confirmed'
					      });

						$state.go('app.home');

					}, function(response){
					    //error
						console.log('ERROR OCCURED IN CALCULATING DISTANCE');	
						var alertPopup = $ionicPopup.alert({
					         title: 'Order',
					         template: 'Error Occured'
					      });	    
					});


		      };

  
})


.controller('OnlinePlaceOrderCtrl', function($scope, $state, $ionicModal, $timeout, $http, $ionicLoading, $ionicPopup ) {

	
	$scope.orders = [];
	$scope.groups = [];
	$scope.category = [];

	

	$scope.data = [{"category":"Lassi","items":{"0":"Sweet Lassi,300","1":"Salt Lassi,30","2":"Roohafza Lassi,40","3":"Strawberry Lassi,40","4":"Alphonso Mango Lassi,40","5":"Fruit Lassi,50","6":"Mango Banana Lassi,50","7":"Black Currant Lassi,50","8":"Pineapple Lassi,50","9":"Chocolate Lassi,50","10":"Butterscotch Lassi,60","11":"Kesar Badam Lassi,60","12":"Pan Lassi,70","13":"Dry Fruit Lassi,70"}},{"category":"Falooda","items":{"0":"Falooda,60","1":"Malai Kulfi Falooda,70","2":"Badami Falooda,80","3":"Chandini Chowk Special,90"}},{"category":"Mocktail","items":{"0":"Vanilla Shake,30","1":"Oreo Shake,40","2":"Banana Lovers,40","3":"Chickoo,40","4":"Snickers,40","5":"Bournvita Shake,40","6":"Fresh Strawberry,40","7":"Chocolate Fountain,50","8":"Mango Alphonso,50","9":"Mixed Fruit Punch,50","10":"Green Pista,50","11":"Mango Strawberry,60","12":"Butterscotch,60","13":"ChocoScotch Schnapps,60","14":"Kesar Badam,60","15":"Vanilla Apple Iceberg,70","16":"Dry Fruit Shake,70","17":"Cashew & Fig Latte,70","18":"Ferrero Rocher,80","19":"Triple Chocolate Fudge,80","20":"Brownie Mud,80"}},{"category":"Coffee","items":{"0":"Rock Island Coffee,50","1":"8PM Coffee,50","2":"Kit Kat Coffee,60","3":"Coffee Italia,60","4":"Free Love(with Brownie),80"}},{"category":"Body Cooler","items":{"0":"Roohafza on Rocks,30","1":"Mint Lime Cursher,50","2":"Thandai,50","3":"Rose Milk,50"}},{"category":"Creamy Zone","items":{"0":"Fruit IceBurg,90","1":"Cookies with Cream,90"}},{"category":"Juice","items":{"0":"Jaljeera,20","1":"Ginger Lime,30","2":"Fresh Lime Soda,30","3":"Blue Curacao Lime,40","4":"Mango Blast,40","5":"WaterMelon Juice,40","6":"Pineapple Juice,40"}},{"category":"Hot Chocolate Fudge","items":{"0":"Single,70","1":"Signature,90","2":"Magnum (500ml CUP),120"}},{"category":"Ice Creams","items":{"0":"Vanilla ice cream,30","1":"Strawberry ice cream,30","2":"Chocolate Ice Cream,40","3":"Butter scotch ice cream,40","4":"Mango ice cream,40","5":"Black Currant ice cream,50","6":"Pista ice cream,50","7":"Fig N Honey ice cream,50","8":"Vanilla Choco Chips,50","9":"Café mocha ice cream,50","10":"Dry fruit ice cream,60","11":"Paan Masala,60"}}];

	  for (var i=0; i<$scope.data.length; i++) {
	    
	    $scope.groups[i] = {
	      name: $scope.data[i].category,
	      items: $scope.data[i].items,
	      show: false
	    };
	  }
	   
	   /*
	   * if given group is the selected group, deselect it
	   * else, select the given group
	   */
	  $scope.toggleGroup = function(group) {
	    group.show = !group.show;
	  };
	  $scope.isGroupShown = function(group) {
	    return group.show;
	  };



	  $scope.plusQty = function(item) {	  	        
        $scope.orders.push(item.split(",")[0]);
        console.log('FINAL: '+ $scope.orders);

        $scope.qtyTotal = {
            'display': 'block'
          }

          $scope.qtyList = {
            'display': 'block'
          }

          var myEl = angular.element(document.querySelector('#qty'));
			myEl.append('<p style="color:#990000;font-weight:bold;font-size:120%;" id="_'+item.split(",")[1]+'"><b style="float:left;">'+item.split(",")[0]+'</b>&nbsp;<b style="float:right;">Rs.'+item.split(",")[1]+'</b></p>');     	

			/*total update*/			
			var myTotal = angular.element(document.querySelector('#total'));
			var old = parseFloat(myTotal.html());

			var newVal = old + parseFloat(item.split(",")[1]);
			myTotal.html(newVal);


      };

      $scope.minusQty = function(item, index) {        
        /*$scope.orders.push(item.split(",")[0]);*/

        for(var i = $scope.orders.length; i >= 0; i--){
	        if($scope.orders[i] == item.split(",")[0]){
	            $scope.orders.splice(i,1);

	            /*total update*/
	            var myTotal = angular.element(document.querySelector('#total'));
				var old = parseFloat(myTotal.html());

				var newVal = old - parseFloat(item.split(",")[1]);
				myTotal.html(newVal);

	            /*break;*/
	        }
	    }
        
        console.log('LENGTH: '+ $scope.orders.length);

	    if ($scope.orders.length == 0) {
	    	$scope.qtyList = {
            'display': 'none'
          }

        $scope.qtyTotal = {
            'display': 'none'
          }
	    }

	    		/*remove element */
	            var myElement = angular.element(document.querySelector('#_'+item.price));
	    		myElement.empty();
	    		/*remove element */

        console.log('FINAL: '+ $scope.orders);
      };

      $scope.hide = function(){
		        $ionicLoading.hide();
		  };

      $scope.show = function() {

				$ionicLoading.show({
				      template: '<p>Order Successfully Placed, Your order will be fulfilled within 24 Hours </p>'
				    });
			  };


	  
      
		$scope.showPopupCash = function() {
				
			      var confirmPopup = $ionicPopup.confirm({
			         title: 'Confirm Order Submission',
			         template: 'Do you want to place order?'
			      });

			      confirmPopup.then(function(res) {
			         if(res) {
			             			$http({
								    method:'GET',				    
								    url:'http://admin.lassicorner.com/services/saveUserOrders.php?orders='+$scope.orders.toString()+'&comments='+$scope.data.comments+'&orderBy='+window.localStorage.getItem('username')+'&outlet='+window.localStorage.getItem('outlet')+'&total='+angular.element(document.querySelector('#total')).html()+'&status=true&balance='+angular.element(document.querySelector('#total')).html()+'&deliveryaddress=test'
								}).then(function(response){	   

									console.log(response.data);
								    
								    if (response.data.search('success') != -1) {
								        $scope.show();


								        	/*$http({
												    method:'GET',				    
												    url:'http://smslane.com/vendorsms/pushsms.aspx?user=abc&password=xyz&msisdn=919898xxxxxx&sid=DEMOID&msg=test%20message&fl=0'
												}).then(function(response){	   
													$state.go('app.home');
													console.log(response.data);

												}, function(response){
												    //error
													console.log('ERROR IN SMS SENDING');		    
												});*/
												$ionicLoading.show({ template: 'Order placed successfully', noBackdrop: true, duration: 3000 });
								    	$state.go('app.home');
								    }
								    if (response.data == "failed") {
								    	
								    	$ionicLoading.show({ template: 'Error occured in placing order please try again.', noBackdrop: true, duration: 3000 });								    	
								    }

								    
								    

								}, function(response){
								    //error
									console.log('ERROR OCCURED IN CALCULATING DISTANCE');		    
								});
			         } else {
			            console.log('Not sure!');
			         }
			      });
			
	   };


	   $scope.showPopupOnline = function() {
				
			      var confirmPopup = $ionicPopup.confirm({
			         title: 'Confirm Order Submission',
			         template: 'Do you want to place order?'
			      });

			      confirmPopup.then(function(res) {
			         if(res) {
			             			console.log('OnlinePayment');
			             			$http({
								    method:'GET',				    
								    url:'http://admin.lassicorner.com/services/saveUserOrders.php?orders='+$scope.orders.toString()+'&comments='+$scope.data.comments+'&orderBy='+window.localStorage.getItem('username')+'&outlet='+window.localStorage.getItem('outlet')+'&total='+angular.element(document.querySelector('#total')).html()+'&status=true&balance='+angular.element(document.querySelector('#total')).html()+'&deliveryaddress=test'
								}).then(function(response){	   

									console.log('OnlinePayment:'+response.data);

									window.open('http://lassicorner.com/ccavenue/index.php?tid=12345&merchant_id=154220&order_id=12345&amount='+angular.element(document.querySelector('#total')).html()+'&&currency=INR&redirect_url=http://lassicorner.com/ccavenue/ccavResponseHandler.php&cancel_url=http://admin.lassicorner.com/ccavResponseHandler.php&language=EN','_blank'); 
								    

								}, function(response){
								    //error
									console.log('ERROR OCCURED IN CALCULATING DISTANCE');		    
								});
			         } else {
			            console.log('Not sure!');
			         }
			      });
			
	   };

  
})


.controller('OnlinePlaceOrderCtrlFranchisee', function($scope, $state, $ionicModal, $timeout, $http, $ionicLoading, $ionicPopup) {

	
	$scope.orders = [];
	$scope.data = [];







	/*by default hide the items list*/
	$scope.itemsSelector = {
            'display': 'none',
          }



/*get list of franchisee purchase item*/
    		$http({
			    method:'GET',				    
			    url:'http://admin.lassicorner.com/services/getFranList.php'
			}).then(function(response){
			    $scope.data = response.data;

			}, function(response){
			    var alertPopup = $ionicPopup.alert({
				      title: '<b>Failed</b>',
				      template: 'Error occured please try again...'
				    });
				
			});
/*get list of franchisee purchase item*/

	   $scope.plusQty = function(item, index) {	 
	   	$scope.qtyList = {
            'display': 'block'
          }

        $scope.qtyTotal = {
            'display': 'block'
          }

        $scope.orders.push(item.name);
        
        console.log('FINAL: '+ $scope.orders);
        

        	var myEl = angular.element(document.querySelector('#qty'));
			myEl.append('<p style="color:#990000;font-weight:bold;font-size:120%;" id="_'+item.price+'"><b style="float:left;">'+item.name+'</b>&nbsp;<b style="float:right;">Rs.'+item.price+'</b></p>');     	

			/*total update*/			
			var myTotal = angular.element(document.querySelector('#total'));
			var old = parseFloat(myTotal.html());

			var newVal = old + parseFloat(item.price);
			myTotal.html(newVal);




      };

      $scope.minusQty = function(item, index) {               

      	
         for(var i = $scope.orders.length; i >= 0; i--){
	        if($scope.orders[i] == item.name){
	            $scope.orders.splice(i,1);

	            /*total update*/
	            var myTotal = angular.element(document.querySelector('#total'));
				var old = parseFloat(myTotal.html());

				var newVal = old - parseFloat(item.price);
				myTotal.html(newVal);

	            /*break;*/
	        }
	    }

	    console.log('LENGTH: '+ $scope.orders.length);

	    if ($scope.orders.length == 0) {
	    	$scope.qtyList = {
            'display': 'none'
          }

        $scope.qtyTotal = {
            'display': 'none'
          }
	    }

	    		/*remove element */
	            var myElement = angular.element(document.querySelector('#_'+item.price));
	    		myElement.empty();
	    		/*remove element */

        console.log('FINAL: '+ $scope.orders);
      };



      $scope.hide = function(){
		        $ionicLoading.hide();
		  };

      $scope.show = function() {

				$ionicLoading.show({
				      template: '<p>Order Successfully Placed, Your order will be fulfilled within 24 Hours</p>'
				    });
			  };



      
		$scope.showPopupCash = function() {
				
			      var confirmPopup = $ionicPopup.confirm({
			         title: 'Confirm Order Submission',
			         template: 'Do you want to place order?'
			      });

			      confirmPopup.then(function(res) {
			         if(res) {
			             			$http({
								    method:'GET',				    
								    url:'http://admin.lassicorner.com/services/saveFranOrders.php?orders='+$scope.orders.toString()+'&comments='+$scope.data.comments+'&orderBy='+window.localStorage.getItem('username')+'&outlet='+window.localStorage.getItem('outlet')+'&total='+angular.element(document.querySelector('#total')).html()+'&status=true&balance='+angular.element(document.querySelector('#total')).html()+''
								}).then(function(response){	   

									debugger;						
									/* search('success')	*/		    
								    if (response.data.search('success') != -1) {
								        
								    		$scope.show();

								        	/*$http({
												    method:'GET',				    
												    url:'http://smslane.com/vendorsms/pushsms.aspx?user=abc&password=xyz&msisdn=919898xxxxxx&sid=DEMOID&msg=test%20message&fl=0'
												}).then(function(response){	   
													$state.go('app.home');
													console.log(response.data);

												}, function(response){
												    //error
													console.log('ERROR IN SMS SENDING');		    
												});*/
												$ionicLoading.show({ template: 'Order placed successfully, Your order will be delivered within 24 Hour.', noBackdrop: true, duration: 3000 });
								    	$state.go('app.home');
								    }
								    if (response.data == "failed") {
								    	
								    	$ionicLoading.show({ template: 'Error occured in placing order please try again.', noBackdrop: true, duration: 3000 });								    	
								    }

								    
								    

								}, function(response){
								    //error
								    $ionicLoading.show({ template: 'Error occured in placing order please try again.', noBackdrop: true, duration: 3000 });								    	
									console.log('ERROR OCCURED IN CALCULATING DISTANCE');		    
								});
			         } else {
			            console.log('Not sure!');
			         }
			      });
			
	   };


	   $scope.showPopupOnline = function() {
				
			      var confirmPopup = $ionicPopup.confirm({
			         title: 'Confirm Order Submission',
			         template: 'Do you want to place order?'
			      });

			      confirmPopup.then(function(res) {
			         if(res) {
			             			console.log('OnlinePayment');
			             			$http({
								    method:'GET',				    
								    url:'http://admin.lassicorner.com/services/saveFranOrders.php?orders='+$scope.orders.toString()+'&comments='+$scope.data.comments+'&orderBy='+window.localStorage.getItem('username')+'&outlet='+window.localStorage.getItem('outlet')+'&total='+angular.element(document.querySelector('#total')).html()+'&status=false&balance=0'
								}).then(function(response){	   

									console.log('OnlinePayment:'+response.data);

									/*?tid=12345&merchant_id=154220&order_id=12345&amount=1.00&&currency=INR&redirect_url=http://admin.lassicorner.com/ccavResponseHandler.php&cancel_url=http://admin.lassicorner.com/ccavResponseHandler.php&language=EN*/

									window.open('http://lassicorner.com/ccavenue/index.php?tid=12345&merchant_id=154220&order_id=12345&amount='+angular.element(document.querySelector('#total')).html()+'&&currency=INR&redirect_url=http://lassicorner.com/ccavenue/ccavResponseHandler.php&cancel_url=http://lassicorner.com/ccavenue/ccavResponseHandler.php&language=EN','_blank'); 
								    

								}, function(response){
								    //error
									console.log('ERROR OCCURED IN CALCULATING DISTANCE');		    
								});
			         } else {
			            console.log('Not sure!');
			         }
			      });
			
	   };



  
});
